package com.haladyj.SS5.security.filter;

import com.haladyj.SS5.security.authentication.CustomAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain)
            throws ServletException, IOException {

        String authorization = request.getHeader("Authorization");

        var a  = new CustomAuthentication(authorization,null);

        try{
            Authentication result = authenticationManager.authenticate(a);

            if(result.isAuthenticated()){
                SecurityContextHolder.getContext().setAuthentication(result);
                chain.doFilter(request,response);
            }
        } catch (BadCredentialsException exception){
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }


    }
}